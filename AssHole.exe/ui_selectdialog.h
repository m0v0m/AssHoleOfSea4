/********************************************************************************
** Form generated from reading UI file 'selectdialog.ui'
**
** Created by: Qt User Interface Compiler version 4.8.6
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_SELECTDIALOG_H
#define UI_SELECTDIALOG_H

#include <QtCore/QVariant>
#include <QtGui/QAction>
#include <QtGui/QApplication>
#include <QtGui/QButtonGroup>
#include <QtGui/QDialog>
#include <QtGui/QDialogButtonBox>
#include <QtGui/QHeaderView>
#include <QtGui/QLabel>
#include <QtGui/QPushButton>

QT_BEGIN_NAMESPACE

class Ui_SelectDialog
{
public:
    QDialogButtonBox *buttonBox;
    QPushButton *LeftChButton;
    QPushButton *RightChButton;
    QLabel *label;

    void setupUi(QDialog *SelectDialog)
    {
        if (SelectDialog->objectName().isEmpty())
            SelectDialog->setObjectName(QString::fromUtf8("SelectDialog"));
        SelectDialog->resize(322, 132);
        buttonBox = new QDialogButtonBox(SelectDialog);
        buttonBox->setObjectName(QString::fromUtf8("buttonBox"));
        buttonBox->setEnabled(true);
        buttonBox->setGeometry(QRect(100, 100, 221, 41));
        buttonBox->setOrientation(Qt::Horizontal);
        buttonBox->setStandardButtons(QDialogButtonBox::Cancel|QDialogButtonBox::Ok);
        LeftChButton = new QPushButton(SelectDialog);
        LeftChButton->setObjectName(QString::fromUtf8("LeftChButton"));
        LeftChButton->setGeometry(QRect(30, 50, 100, 50));
        RightChButton = new QPushButton(SelectDialog);
        RightChButton->setObjectName(QString::fromUtf8("RightChButton"));
        RightChButton->setGeometry(QRect(160, 50, 100, 50));
        label = new QLabel(SelectDialog);
        label->setObjectName(QString::fromUtf8("label"));
        label->setGeometry(QRect(20, 10, 291, 21));

        retranslateUi(SelectDialog);
        QObject::connect(buttonBox, SIGNAL(accepted()), SelectDialog, SLOT(accept()));
        QObject::connect(buttonBox, SIGNAL(rejected()), SelectDialog, SLOT(reject()));

        QMetaObject::connectSlotsByName(SelectDialog);
    } // setupUi

    void retranslateUi(QDialog *SelectDialog)
    {
        SelectDialog->setWindowTitle(QApplication::translate("SelectDialog", "Dialog", 0, QApplication::UnicodeUTF8));
        LeftChButton->setText(QApplication::translate("SelectDialog", "YES", 0, QApplication::UnicodeUTF8));
        RightChButton->setText(QApplication::translate("SelectDialog", "NO", 0, QApplication::UnicodeUTF8));
        label->setText(QApplication::translate("SelectDialog", "Do you want reboot this client like other?", 0, QApplication::UnicodeUTF8));
    } // retranslateUi

};

namespace Ui {
    class SelectDialog: public Ui_SelectDialog {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_SELECTDIALOG_H
