/********************************************************************************
** Form generated from reading UI file 'dialog.ui'
**
** Created by: Qt User Interface Compiler version 4.8.6
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_DIALOG_H
#define UI_DIALOG_H

#include <QtCore/QVariant>
#include <QtGui/QAction>
#include <QtGui/QApplication>
#include <QtGui/QButtonGroup>
#include <QtGui/QDialog>
#include <QtGui/QHeaderView>
#include <QtGui/QLineEdit>
#include <QtGui/QPushButton>

QT_BEGIN_NAMESPACE

class Ui_Dialog
{
public:
    QPushButton *SysCmd;
    QLineEdit *CmdLine;
    QPushButton *AllReboot;

    void setupUi(QDialog *Dialog)
    {
        if (Dialog->objectName().isEmpty())
            Dialog->setObjectName(QString::fromUtf8("Dialog"));
        Dialog->resize(346, 132);
        SysCmd = new QPushButton(Dialog);
        SysCmd->setObjectName(QString::fromUtf8("SysCmd"));
        SysCmd->setGeometry(QRect(100, 10, 100, 100));
        CmdLine = new QLineEdit(Dialog);
        CmdLine->setObjectName(QString::fromUtf8("CmdLine"));
        CmdLine->setGeometry(QRect(30, 40, 50, 50));
        AllReboot = new QPushButton(Dialog);
        AllReboot->setObjectName(QString::fromUtf8("AllReboot"));
        AllReboot->setGeometry(QRect(230, 10, 100, 100));

        retranslateUi(Dialog);

        QMetaObject::connectSlotsByName(Dialog);
    } // setupUi

    void retranslateUi(QDialog *Dialog)
    {
        Dialog->setWindowTitle(QApplication::translate("Dialog", "Dialog", 0, QApplication::UnicodeUTF8));
        SysCmd->setText(QApplication::translate("Dialog", "System CMD", 0, QApplication::UnicodeUTF8));
        AllReboot->setText(QApplication::translate("Dialog", "All Reboot", 0, QApplication::UnicodeUTF8));
    } // retranslateUi

};

namespace Ui {
    class Dialog: public Ui_Dialog {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_DIALOG_H
