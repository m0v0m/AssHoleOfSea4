﻿#include "selectdialog.h"
#include "ui_selectdialog.h"

SelectDialog::SelectDialog(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::SelectDialog)
{
    ui->setupUi(this);
    setWindowTitle("selection");

    ui->LeftChButton->setText("YES");
    ui->RightChButton->setText("NO");
}

SelectDialog::~SelectDialog()
{
    delete ui;
}

void SelectDialog::on_LeftChButton_clicked()
{
   system("sudo reboot");
   reject();
}

void SelectDialog::on_RightChButton_clicked()
{
    reject();
}
