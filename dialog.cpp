﻿#include "dialog.h"
#include "ui_dialog.h"

Dialog::Dialog(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::Dialog)
{
    ui->setupUi(this);
     setWindowTitle("AssHole");

     //智文a網路模組第一版
     AllIP = "192.168.180.255";
     LocalIP = getIpAdress();
     //LocalIP = "192.168.180.20"; //only for test 上系統時由上一行指令取得該硬體IP
     qDebug() << LocalIP;
     stand_ID=LocalIP.right(2);
     //將各位數的十位數塞0
     (stand_ID[0] == '.') ? stand_ID[0] = '0':stand_ID[0] =stand_ID[0] ;
     istand_ID=stand_ID.toInt();
     portout2GUI= 1111;
     portin = 2222;
     udpSocket = new QUdpSocket(this);
     connect(udpSocket,SIGNAL(readyRead()),this,SLOT(dataReceived()));
     udpSocket->bind(portin);
     //智文a網路模組第一版_END

     //設定一個週期處發的時間事件
     QTimer *timer = new QTimer(this);
     //关联定时器溢出信号和相应的槽函数
     connect(timer, SIGNAL(timeout()), this, SLOT(timerUpdate()));
     timer->start(200);//更新速度1000為一秒鍾
     //設定一個週期處發的時間事件_END


}

Dialog::~Dialog()
{
    delete ui;
}

void Dialog::timerUpdate()
{
    //get input cmd
    QString str;
    str = ui->CmdLine->text();
    QByteArray ba = str.toLatin1();
    const char *c_str2 = ba.data();
    sendData2GUI.cSysCmd= *c_str2;
    //get input cmd_END
}

void Dialog::on_SysCmd_clicked()
{
    sendData2GUI.CommandOfID = '$';
    //智文a網路模組第一版
    ZeroMemory(buf_to_net, sizeof buf_to_net);
    CopyMemory(buf_to_net, &sendData2GUI, sizeof sendData2GUI);
    for(int i=0;i<1024;i++) buf_register[i] = buf_to_net[i];
    QHostAddress serverAddress = QHostAddress(AllIP);
    udpSocket->writeDatagram((char *)buf_to_net,sizeof(buf_to_net),serverAddress,portout2GUI);//傳出網路封包的cmd
    //智文a網路模組第一版_END

}

void Dialog::on_AllReboot_clicked()
{
    //發出全系統重新啟動的人要將自己接收的port轉成發送的port
    portout2AssHole=portin;
    portin=3333;
    delete udpSocket;
    udpSocket = new QUdpSocket(this);
    connect(udpSocket,SIGNAL(readyRead()),this,SLOT(dataReceived()));
    udpSocket->bind(portin);
    //發出全系統重新啟動的人要將自己接收的port轉成發送的port_end

    sendData2AssHole.CommandOfID = '$';
    sendData2AssHole.cSysCmd = 'R';
    //智文a網路模組第一版
    ZeroMemory(buf_to_net, sizeof buf_to_net);
    CopyMemory(buf_to_net, &sendData2AssHole, sizeof sendData2AssHole);
    for(int i=0;i<1024;i++) buf_register[i] = buf_to_net[i];
    QHostAddress serverAddress = QHostAddress(AllIP);
    udpSocket->writeDatagram((char *)buf_to_net,sizeof(buf_to_net),serverAddress,portout2AssHole);//傳出網路封包的cmd
    //智文a網路模組第一版_END

    //將其他人的重起指令發出去後自己也關機
    SelectDialog *dlg= new SelectDialog(this);
    dlg->show();
}

//取得IP位址
QString Dialog::getIpAdress()
{
    //取得網卡資訊
    QList<QNetworkInterface> list = QNetworkInterface::allInterfaces();
         foreach(QNetworkInterface interface,list)
         {
             qDebug() <<"Device:"<<interface.name();
             qDebug() << "HardwareAddress:"<<interface.hardwareAddress();


             QList<QNetworkAddressEntry> entryList = interface.addressEntries();
          //取得IP位址清單,並包含一個IP位址,一個子遮罩及一個廣播位址
             foreach(QNetworkAddressEntry entry,entryList)
             {
                 qDebug()<<"IP Address:"<<entry.ip().toString();
                 qDebug()<<"Netmask: "  <<entry.netmask().toString();
                 qDebug()<<"Broadcast:" <<entry.broadcast().toString();
                 //請先用ifconfig確認網卡是eth0還是eth1或其他
                 if(interface.name() == "eth0") return entry.ip().toString() ;
             }
     }
         return 0;
}
//取得IP位址_END

//智文a網路模組第一版
void Dialog::dataReceived()
{
    while(udpSocket->hasPendingDatagrams())
    {
        udpSocket->readDatagram((char*)buf_from_net, sizeof (buf_from_net));
    }
    //判斷是'$'的系統命令封包,或'@'的通道名稱封包，還是'#'的管制表資料封包
    if(buf_from_net[0]=='$')
    {
        CMD_FROM_AssHole *pDataIn = NULL;
        pDataIn = (CMD_FROM_AssHole*) buf_from_net;
        CopyMemory( &(recvCMD), &(pDataIn->CpInCMD), sizeof(CPIN_CMD) );
        //收到字元R執行重新啟動
        if(recvCMD.CommandOfAssHole == 'R') system("sudo reboot");
    }
}
//智文a網路模組第一版_END
