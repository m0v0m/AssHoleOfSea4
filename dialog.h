﻿#ifndef DIALOG_H
#define DIALOG_H

#include <QDialog>
#include <QTimer>
#include<QNetworkInterface>
#include "selectdialog.h"

//智文a網路模組第一版
#include "global.h"
#include <QUdpSocket>
#define RtlCopyMemory(Destination,Source,Length) memcpy((Destination),(Source),(Length))
#define RtlZeroMemory(Destination,Length) memset((Destination),0,(Length))
#define CopyMemory RtlCopyMemory
#define ZeroMemory RtlZeroMemory
//智文a網路模組第一版_END

namespace Ui {
class Dialog;
}

class Dialog : public QDialog
{
    Q_OBJECT

public:
    explicit Dialog(QWidget *parent = 0);
    ~Dialog();

private slots:
    void on_SysCmd_clicked();

    void on_AllReboot_clicked();

    QString getIpAdress();//取得IP位址

    void timerUpdate();

    //智文a網路模組第一版
    void dataReceived();

private:
    Ui::Dialog *ui;

    //智文a網路模組第一版
    CPOUT sendData2GUI;
    CPOUT sendData2AssHole;
    CPIN_CMD recvCMD;
    QUdpSocket *udpSocket;
    QString LocalIP;
    QString AllIP;
    QString stand_ID;
    int istand_ID;
    int portout2GUI;
    int portout2AssHole;
    int portin;
    char buf_to_net[1024];
    char buf_register[1024];
    char buf_from_net[2048];
    //智文a網路模組第一版_END
};

#endif // DIALOG_H
